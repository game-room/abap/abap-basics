CLASS zcl_jega_07_iterations DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_JEGA_07_ITERATIONS IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.

* Declarations **********************************************************************
* CONSTANTS c_number TYPE i VALUE 3.
* CONSTANTS c_number TYPE i VALUE 5.
    CONSTANTS c_number TYPE i VALUE 10.
    DATA number TYPE i.

* Example 1: DO ... ENDDO with TIMES **********************************************************************
    out->write( `----------------------------------` ).
    out->write( `Example 1: DO ... ENDDO with TIMES` ).
    out->write( `----------------------------------` ).
    DO c_number TIMES.
      out->write( `Hello World` ).
    ENDDO.

* Example 2: DO ... ENDDO with Abort Condition **********************************************************************
    out->write( `-------------------------------` ).
    out->write( `Example 2: With Abort Condition` ).
    out->write( `-------------------------------` ).
    number = c_number * 2.
    " count backwards from number to c_number.
    DO.
      out->write( |{ sy-index }: Value of number: { number }| ).
      number = number - 1.
      "abort condition
      IF number <= c_number.
        EXIT.
      ENDIF.
    ENDDO.


* Example 3: WHILE ... ENDEHILE **********************************************************************
    out->write( `-------------------------------` ).
    out->write( `Example 3: WHILE` ).
    out->write( `-------------------------------` ).
    DATA: gv_numero    TYPE n LENGTH 2,
          gv_resultado TYPE n LENGTH 2.

    gv_numero = c_number.
    WHILE gv_numero < 20.
      gv_numero += 1.
      gv_resultado = 2 * gv_numero.
      out->write( |{ sy-index }: Value of number: { gv_resultado }| ).
    ENDWHILE.

  ENDMETHOD.
ENDCLASS.
