CLASS zcl_jega_11_methods DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_JEGA_11_METHODS IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.


    CONSTANTS c_carrier_id TYPE /dmo/carrier_id VALUE 'LH'.
    CONSTANTS c_connection_id TYPE /dmo/connection_id VALUE '0400'.

    DATA carrier_id TYPE /dmo/carrier_id.
    DATA connection_id TYPE /dmo/connection_id.
    DATA connection TYPE REF TO lcl_connection.
    DATA connections TYPE TABLE OF REF TO lcl_connection.

* Create Instance **********************************************************************
    connection = NEW #( ).
* Call Method and Handle Exception **********************************************************************
    out->write( |i_carrier_id = '{ c_carrier_id }' | ).
    out->write( |i_connection_id = '{ c_connection_id }'| ).
    TRY.
        connection->set_attributes(
            EXPORTING
                i_carrier_id = c_carrier_id
                i_connection_id = c_connection_id ).
        APPEND connection TO connections.
        out->write( `Method call successful` ).


        LOOP AT connections INTO DATA(connection_item).
          connection_item->get_attributes(
              IMPORTING
              e_carrier_id = carrier_id
              e_connection_id = connection_id ).

          out->write( |Flight info: { carrier_id } { connection_id }| ).

          " Easy string table print
          " out->write( connection->get_output( ) ).
          " in a value assignment (with inline declaration for result)
          DATA(result) = connection->get_output( ).

          " in logical expression
          IF result IS NOT INITIAL.

            " as operand in a statement
            LOOP AT result INTO DATA(line).
              " to supply input parameter of another method
              out->write( line ).
            ENDLOOP.
          ENDIF.

        ENDLOOP.
      CATCH cx_abap_invalid_value.
        out->write( `Method call failed` ).
    ENDTRY.
  ENDMETHOD.
ENDCLASS.
