CLASS zcl_jega_10_global_local_class DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_JEGA_10_GLOBAL_LOCAL_CLASS IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.

    DATA connection TYPE REF TO lcl_connection.
    DATA connections TYPE TABLE OF REF TO lcl_connection.

    connection = NEW lcl_connection( ).
    connection->conn_counter = 1.
    connection->carrier_id = 'LH'.
    connection->connection_id = '0400'.
    APPEND connection TO connections.

    connection = NEW #( ).
    connection->carrier_id = 'AA'.
    connection->connection_id = '0017'.
    APPEND connection TO connections.

    connection = NEW #( ).
    connection->carrier_id = 'SQ'.
    connection->connection_id = '0001'.
    APPEND connection TO connections.


    connection->conn_counter = 3.

* Example ENDLOOP **********************************************************************
    out->write( `-----------------------------` ).
    out->write( `Example lcl class: instances` ).
    out->write( `-----------------------------` ).
    LOOP AT connections INTO DATA(connection_item).
      out->write( |Row: { sy-tabix } connection-id: { connection_item->connection_id } carrier-id: { connection_item->carrier_id } counter: { connection_item->conn_counter } | ).
    ENDLOOP.

  ENDMETHOD.
ENDCLASS.
