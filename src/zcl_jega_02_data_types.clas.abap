CLASS zcl_jega_02_data_types DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_JEGA_02_DATA_TYPES IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.


    DATA flights TYPE /dmo/t_flight.

    DATA: gv_saludo  TYPE string,
          gv_mensaje TYPE string,
          gv_fecha   TYPE d.

    gv_fecha = '20231101'.
    gv_saludo = 'Hola: '.
    FINAL(ts) = cl_abap_context_info=>get_system_date( ).
    CONCATENATE gv_saludo gv_fecha INTO gv_mensaje RESPECTING BLANKS.

    out->write( gv_mensaje ).
    out->write( sy-tzone ).
    out->write( ts ).

* Data Objects with Built-in Types
**********************************************************************
    " comment/uncomment the following declarations and check the output
*    DATA variable TYPE string.
* DATA variable TYPE i.
    DATA variable TYPE d.
* DATA variable TYPE c LENGTH 10.
* DATA variable TYPE n LENGTH 10.
* DATA variable TYPE p LENGTH 8 DECIMALS 2.

* Output
**********************************************************************
    out->write( 'Result with Initial Value' ).
    out->write( variable ).
    out->write( '---------' ).
    variable = '19891109'.
    out->write( 'Result with Value 19891109' ).
    out->write( variable ).
    out->write( '---------' ).



    DATA var_date TYPE d.
    DATA var_pack TYPE p LENGTH 3 DECIMALS 2.
    DATA var_string TYPE string.
    DATA var_char TYPE c LENGTH 3.


    var_pack = 1 / 8.
    out->write( |1/8 = { var_pack NUMBER = USER }| ).


    TRY.
        var_pack = EXACT #( '0.13' ).
      CATCH cx_sy_conversion_error.
        out->write( |1/8 has to be rounded. EXACT triggered an exception  { EXACT #( 1 / 8 ) } |  ).
    ENDTRY.


    var_string = 'ABCDE'.
    var_Char = var_string.
    out->write( var_char ).


    TRY.
        var_char = EXACT #( var_string ).
      CATCH cx_sy_conversion_error.
        out->write( 'String has to be truncated. EXACT triggered an exception' ).
    ENDTRY.


    var_date = 'ABCDEFGH'.
    out->write( var_Date ).


    TRY.
        var_date = EXACT #( 'ABCDEFGH' ).
      CATCH cx_sy_conversion_error.
        out->write( |ABCDEFGH is not a valid date. EXACT triggered an exception| ).
    ENDTRY.


    var_date = '20221232'.
    out->write( var_date ).

  ENDMETHOD.
ENDCLASS.
